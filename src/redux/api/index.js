import axios from 'axios';

// const URL_PATH = 'https://api.themoviedb.org';
// const API_KEY = '9f256582b4040e6fc78b7a6ca742ccb7';

const baseURL = 'https://www.omdbapi.com/?apiKey=ffd0c3a5';

export const apiCall = (url, data, headers, method) => axios({
    method,
    url: baseURL + url,
    data,
    headers
});