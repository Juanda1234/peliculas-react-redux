import { makeStyles } from '@material-ui/styles';

export default makeStyles({
    cardContainer: {
        marginBotton: 8
    },
    poster: {
        width: 120
    },
    titulos: {
        paddingLeft: 8
    }
})